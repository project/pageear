/********************************************************************************************
* PageEar javascript configuration file
*
* @author    Fernando San Julián
*********************************************************************************************/

// swf files
var flagSwf = 'flag.swf';
var peelSwf = 'turn.swf';
var waitEnabled = 'true';
var waitURL = 'wait.gif';
var waitWidth = '42';
var waitHeight = '42';

/********************************************************************************************
* PageEar parameters. This shows its acceptable values and its defaults.
*
* flag style. Values: 'style1', 'style2'.
*   flagStyle = 'style1';
* peel style. Values: 'style1', 'style2'.
*   peelStyle = 'style1';
*
* flag and peel sizes
*   flagWidth = 100;
*   flagHeight = 100;
*   peelWidth = 500;
*   peelHeight = 500;
*
* Position of ear. Values: 'topleft', 'topright', 'bottomleft' or 'bottomright'.
*   peelPosition = 'topright';
* Position model. Values: 'absolute' or 'fixed'.
*   peelPositionModel = 'absolute';
*
* URL of small image.
*   smallURL = 'small.jpg';
* URL of big image.
*   bigURL = 'big.jpg';
* The image is mirrored on peel. Values: 0 (false) or 1 (true).
*   mirror = 0;
* Start transition. Values: 'none', 'Blinds', 'Fade', 'Fly', 'Iris', 'Photo', 'Rotate', 'Squeeze', 'Wipe', 'PixelDissolve', 'Zoom'.
*   inTransition = 'Photo';
* Transition duration. Values: 1-9
*   transitionDuration = 4;
*
* Color of Peel. Values: 'golden', 'silver', 'custom'
*   peelColor = 'golden';
* Color of Peel Style. Values: 'flat', 'gradient'
*   peelColorStyle = 'gradient';
* RGB values for a Custom Peel Color. Values: 0-255.
*   redValue = 0;
*   greenValue = 0;
*   blueValue = 0;
*
* enable or disable link. Values: 0 (disabled) or 1 (enabled).
*   linkEnabled = 0;
* Where to open the link. Same or New Window (tab). Values: '_self' or '_blank'.
*   linkTarget = '_blank';
* URL of link.
*   link = 'http://www.manfer.co.cc';
*
* URL of sound on load Peel Ad.
*   loadSoundURL = '';
* URL of sound when peel is opened.
*   openSoundURL = '';
* URL of sound when peel is closed.
*   closeSoundURL = '';
*
* Speed of flag movement. Values: 1-9
*   flagSpeed = 4;
* Speed of peel. Values: 1-9
*   peelSpeed = 4;
* Milliseconds till an automatic open of peel. Values: 0 (no automatic open) >0 (milliseconds configured).
*   automaticOpen = 0;
* Milliseconds till an automatic close of peel. Values: 0 (no automatic close) >0 (milliseconds configured).
*   automaticClose = 0;
*
* Close button on open Peel. Values: 0 (disabled) or 1 (enabled).
*   close_button_enable = 0;
* Text on close button.
*   text_on_close_button = 'close';
* RGB values of close button. Values: 0-255.
*   close_redValue = 0;
*   close_greenValue = 0;
*   close_blueValue = 0;
*
*********************************************************************************************/

// Help functions to control pageear flash versions
// to be sure the correct versions are retrieved from
// server and not old ones from browser cache are used.
function getPageearFlagVersion(){
  return 'flag113';
}

function getPageearPeelVersion(){
  return 'turn114';
}

function doPeel(position){
  if (position == 'topleft' || position =='topright') {
    document.getElementById('peelDiv_' + position).style.top = '0px';
    document.getElementById('flagDiv_' + position).style.top = '-1000px';
  } else {
    document.getElementById('peelDiv_' + position).style.bottom = '0px';
    document.getElementById('flagDiv_' + position).style.bottom = '-1000px';
  }
  document.getElementById('peel' + position).doPeel();
}

function doFlag(position){
  if (position == 'topleft' || position =='topright') {
    document.getElementById("flagDiv_" + position).style.top = "0px";
    document.getElementById("peelDiv_" + position).style.top = "-1000px";
  } else {
    document.getElementById("flagDiv_" + position).style.bottom = "0px";
    document.getElementById("peelDiv_" + position).style.bottom = "-1000px";
  }
}

function showFlag(position, flagWidth, flagHeight, peelWidth, peelHeight){
  if (document.getElementById("waitDiv_" + position)) {
    document.getElementById("waitDiv_" + position).style.display = "none";
  }

  document.getElementById("flagDiv_" + position).style.width = flagWidth + "px";
  document.getElementById("flagDiv_" + position).style.height = flagHeight + "px";
  document.getElementById("peelDiv_" + position).style.width = peelWidth + "px";
  document.getElementById("peelDiv_" + position).style.height = peelHeight + "px";

  if (position == 'topleft' || position =='topright') {
    document.getElementById("peelDiv_" + position).style.top = "-1000px";
  } else {
    document.getElementById("peelDiv_" + position).style.bottom = "-1000px";
  }
  document.getElementById("flagDiv_" + position).style.display = "block";
}

function hidePeelFlag(position){
  document.getElementById("flagDiv_" + position).style.width = "1px";
  document.getElementById("flagDiv_" + position).style.height = "1px";
  document.getElementById("peelDiv_" + position).style.width = "1px";
  document.getElementById("peelDiv_" + position).style.height = "1px";
}


function pageear(pageearvars) {

  this.pageearvars = pageearvars;

  this.flagparams = {
    play: "true",
    loop: "true",
    menu: "false",
    quality: "autohigh",
    scale: "noscale",
    wmode: "transparent",
    bgcolor: "#ffffff",
    allowScriptAccess: "sameDomain",
    allowFullScreen: "false"
  };

  this.flagattributes = {
    id: "flag" + this.pageearvars.peelPosition,
    name: "flag" + this.pageearvars.peelPosition,
    styleclass: "pageear"
  };

  this.peelparams = {
    play: "true",
    loop: "true",
    menu: "false",
    quality: "autohigh",
    scale: "noscale",
    wmode: "transparent",
    bgcolor: "#ffffff",
    allowScriptAccess: "sameDomain",
    allowFullScreen: "false"
  };

  this.peelattributes = {
    id: "peel" + this.pageearvars.peelPosition,
    name: "peel" + this.pageearvars.peelPosition,
    styleclass: "pageear"
  };

  this.write = writePageearObjects;
  this.flagvars = getFlagVars;
  this.peelvars = getPeelVars;
  this.flagVersion = getPageearFlagVersion;
  this.peelVersion = getPageearPeelVersion;

}

function getFlagVars () {
  var flagvars = {};
  flagvars.flagStyle = this.pageearvars.flagStyle;
  flagvars.flagWidth = this.pageearvars.flagWidth;
  flagvars.flagHeight = this.pageearvars.flagHeight;
  flagvars.peelPosition = this.pageearvars.peelPosition;
  flagvars.smallURL = this.pageearvars.smallURL;
  flagvars.mirror = this.pageearvars.mirror;
  flagvars.inTransition = this.pageearvars.inTransition;
  flagvars.transitionDuration = this.pageearvars.transitionDuration;
  flagvars.peelColor = this.pageearvars.peelColor;
  flagvars.peelColorStyle = this.pageearvars.peelColorStyle;
  flagvars.redValue = this.pageearvars.redValue;
  flagvars.greenValue = this.pageearvars.greenValue;
  flagvars.blueValue = this.pageearvars.blueValue;
  flagvars.loadSoundURL = this.pageearvars.loadSoundURL;
  flagvars.flagSpeed = this.pageearvars.flagSpeed;
  return flagvars;
}

function getPeelVars() {
  var peelvars = {};
  peelvars.peelStyle = this.pageearvars.peelStyle;
  peelvars.flagWidth = this.pageearvars.flagWidth;
  peelvars.flagHeight = this.pageearvars.flagHeight;
  peelvars.peelWidth = this.pageearvars.peelWidth;
  peelvars.peelHeight = this.pageearvars.peelHeight;
  peelvars.peelPosition = this.pageearvars.peelPosition;
  peelvars.bigURL = this.pageearvars.bigURL;
  peelvars.mirror = this.pageearvars.mirror;
  peelvars.peelColor = this.pageearvars.peelColor;
  peelvars.peelColorStyle = this.pageearvars.peelColorStyle;
  peelvars.redValue = this.pageearvars.redValue;
  peelvars.greenValue = this.pageearvars.greenValue;
  peelvars.blueValue = this.pageearvars.blueValue;
  peelvars.linkEnabled = this.pageearvars.linkEnabled;
  peelvars.linkTarget = this.pageearvars.linkTarget;
  peelvars.link = this.pageearvars.link;
  peelvars.openSoundURL = this.pageearvars.openSoundURL;
  peelvars.closeSoundURL = this.pageearvars.closeSoundURL;
  peelvars.peelSpeed = this.pageearvars.peelSpeed;
  peelvars.automaticOpen = this.pageearvars.automaticOpen;
  peelvars.automaticClose = this.pageearvars.automaticClose;
  peelvars.close_button_enable = this.pageearvars.close_button_enable;
  peelvars.text_on_close_button = this.pageearvars.text_on_close_button.toLowerCase();
  peelvars.close_redValue = this.pageearvars.close_redValue;
  peelvars.close_greenValue = this.pageearvars.close_greenValue;
  peelvars.close_blueValue = this.pageearvars.close_blueValue;
  return peelvars;
}


function writePageearObjects () {
<!--

  // Absolute not available for bottom pageears.
  // Set it always to fixed to prevent a wrong configuration with absolute.
  if (this.pageearvars.peelPosition == 'bottomleft' || this.pageearvars.peelPosition == 'bottomright') {
    this.pageearvars.peelPositionModel = 'fixed';
  }

  // Check position of ear. 
  if(this.pageearvars.peelPosition == 'topleft' || this.pageearvars.peelPosition == 'bottomleft') {
    xPos = 'left';
  } else {
    xPos = 'right';
  }

  if(this.pageearvars.peelPosition == 'topleft' || this.pageearvars.peelPosition == 'topright') {
    yPos = 'top';
  } else {
    yPos = 'bottom';
  }

  if (this.pageearvars.waitEnable == "1") {
    //Write wait div layer
    document.write('<div id="waitDiv_' + this.pageearvars.peelPosition + '" style="position:' + this.pageearvars.peelPositionModel + '; width:' + this.pageearvars.waitWidth +'px; height:' + this.pageearvars.waitHeight + 'px; z-index:9999; ' + xPos + ':0px; ' + yPos + ':0px;">');
    document.write('<img src="' + this.pageearvars.waitURL + '" />');
    document.write('</div>');
  }

  // Write peel div layer
  document.write('<div id="peelDiv_' + this.pageearvars.peelPosition  + '" style="position:' + this.pageearvars.peelPositionModel + '; width:' + this.pageearvars.peelWidth + 'px; height:' + this.pageearvars.peelHeight + 'px; z-index:9999; ' + xPos + ':0px; ' + yPos + ':0px;">');

  document.write('<div id="peelDivAlternative_' + this.pageearvars.peelPosition + '">');
  document.write('<a href="http://www.adobe.com/go/getflashplayer">');
  document.write('<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />');
  document.write('</a>');
  document.write('</div>');

  // Close peel div layer
  document.write('</div>'); 


  // Write flag div layer
  document.write('<div id="flagDiv_' + this.pageearvars.peelPosition  + '" style="position:' + this.pageearvars.peelPositionModel + '; width:' + this.pageearvars.flagWidth + 'px; height:' + this.pageearvars.flagHeight + 'px; z-index:9999; ' + xPos + ':0px; ' + yPos + ':0px;">');

  document.write('<div id="flagDivAlternative_' + this.pageearvars.peelPosition + '">');
  document.write('<a href="http://www.adobe.com/go/getflashplayer">');
  document.write('<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />');
  document.write('</a>');
  document.write('</div>');

  // Close flag div layer
  document.write('</div>');


  // Embed flag
  swfobject.embedSWF(flagSwf + "?" + this.flagVersion(), "flagDivAlternative_" + this.pageearvars.peelPosition, this.pageearvars.flagWidth, this.pageearvars.flagHeight, "9.0.0", "expressInstall.swf", this.flagvars(), this.flagparams, this.flagattributes);

    
  // Embed peel
  swfobject.embedSWF(peelSwf + "?" + this.peelVersion(), "peelDivAlternative_" + this.pageearvars.peelPosition, this.pageearvars.peelWidth, this.pageearvars.peelHeight, "9.0.0", "expressInstall.swf", this.peelvars(), this.peelparams, this.peelattributes);

 
  hidePeelFlag(this.pageearvars.peelPosition);

// -->
}
