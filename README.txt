
-- SUMMARY --

The Drupal pageear module allows you to add pagepeel, "magic corners"  type
banners to your website. 

For a full description of the module, visit the project page:
  http://drupal.org/project/pageear

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/pageear


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  pageear module:

  - administer pageears

    Users in roles with the "administer pageears" permission will be able to
    administer pageears.

  - use PHP for pageear visibility

    Users in roles with the "use PHP for pageear visibility" permission will
    be able to configure pageears visibility using PHP code.

* Configure, add or delete your pageears in Administer >> Site building >>
  Pageears.


-- TROUBLESHOOTING --

* If you find any problem, write an issue in the project issue queu:

  - http://drupal.org/project/issues/pageear


-- FAQ --

Q: 

A: 


-- CONTACT --

Current maintainers:
* Fernando San Julián (manfer) - http://drupal.org/user/506264
